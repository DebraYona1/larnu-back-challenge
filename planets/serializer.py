from rest_framework import serializers

# Serializers define the API representation.
from planets.models import Planet


class PlanetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Planet
        fields = ["id", "name", "satellites", "diameter", "imgUrl"]
