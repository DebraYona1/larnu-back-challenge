from rest_framework import viewsets

# Create your views here.
from planets.models import Planet
from planets.serializer import PlanetSerializer


class PlanetViewSet(viewsets.ModelViewSet):
    queryset = Planet.objects.all().order_by("-diameter")
    serializer_class = PlanetSerializer
