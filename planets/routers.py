from rest_framework import routers

from planets.views import PlanetViewSet

router = routers.DefaultRouter()
router.register(r"planets", PlanetViewSet, basename="planets")
