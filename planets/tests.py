from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase
from planets.models import Planet
from planets.routers import router


class PlanetsTests(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path("api/", include(router.urls)),
    ]

    def setUp(self):
        self.tierra = Planet.objects.create(
            name="Tierra", satellites=3, diameter="231.22"
        )
        self.mercurio = Planet.objects.create(
            name="Mercurio", satellites=3, diameter="231.22"
        )

        self.valid_payload = {
            "name": "Pluton",
            "satellites": 2,
            "diameter": "12.33",
        }
        self.invalid_payload = {
            "name": "",
            "satellites": 4,
            "diameter": "23.44",
        }

    def test_create_planet_valid_data(self):
        url = reverse("planets-list")
        response = self.client.post(url, data=self.valid_payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["name"], "Pluton")
        self.assertEqual(Planet.objects.count(), 3)

    def test_create_planet_invalid_data(self):
        url = reverse("planets-list")
        response = self.client.post(url, data=self.invalid_payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Planet.objects.count(), 2)

    def test_get_planets(self):
        url = reverse("planets-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Planet.objects.count(), 2)

    def test_update_planet(self):
        url = reverse("planets-detail", kwargs={"pk": self.tierra.pk})
        response = self.client.put(url, data=self.valid_payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Planet.objects.count(), 2)

    def test_valid_delete_planet(self):
        response = self.client.delete(
            reverse("planets-detail", kwargs={"pk": self.mercurio.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Planet.objects.count(), 1)
