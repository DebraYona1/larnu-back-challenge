from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Planet(models.Model):
    name = models.CharField(_("name"), max_length=50)
    satellites = models.IntegerField(_("satellites"))
    diameter = models.DecimalField(_("diameter"), max_digits=7, decimal_places=3)
    imgUrl = models.URLField(_("image"), max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = _("Planet")
        verbose_name_plural = _("Planets")

    def __str__(self):
        return self.name
