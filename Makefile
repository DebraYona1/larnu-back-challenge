lint:
	docker-compose build
	docker-compose run --rm web flake8 --exit-zero
	docker-compose down

test:
	docker-compose build
	docker-compose run --rm web python manage.py migrate
	docker-compose up -d
	docker-compose run --rm web coverage run --source='.' manage.py test
	docker-compose run --rm web coverage report
	docker-compose down
	